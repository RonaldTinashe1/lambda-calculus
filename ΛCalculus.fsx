type Term =
    | Variable of string
    | Abstraction of string * Term
    | Application of Term * Term

let βReduce term =
    let rec innerϐReduce scope term =
        let lookup scope variable =
            let freeVariable () = Variable variable
            let value = Map.tryFind variable scope
            scope, Option.defaultWith freeVariable value

        let bind =
            (Map.add: string -> Term -> Map<string, Term> -> Map<string, Term>)

        match term with
        | Variable variable -> lookup scope variable
        | Abstraction _ -> scope, term
        | Application (abstraction, argument) ->
            let scope, abstraction = innerϐReduce scope abstraction
            let scope, argument = innerϐReduce scope argument

            match abstraction with
            | Abstraction (parameter, body) ->
                let scope, body = innerϐReduce scope body
                let scope = bind parameter argument scope
                innerϐReduce scope body
            | _ -> scope, Application(abstraction, argument)

    let _, βNormalForm = innerϐReduce Map.empty term
    βNormalForm
